import os
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from pathlib import Path


class Quality(Enum):
    FULL = 'full'
    DEFAULT = 'default'
    HALF = 'half'
    QUARTER = 'quarter'
    EIGHT = 'eight'


class Webcam(Enum):
    WILDSPITZ = 'https://storage.roundshot.com/5595515f75aba9.83008277'
    RIGI = 'https://storage.roundshot.com/5c1a1db365b684.49402499'


@dataclass
class Configuration:
    time: datetime
    end: datetime
    interval: int = 10
    webcam: Webcam = Webcam.WILDSPITZ
    quality: Quality = Quality.DEFAULT
    save_path: Path = os.getcwd()
