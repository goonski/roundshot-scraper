from typing import Dict

from .time_handling import normalized_now, parse_raw_time
from .types import Configuration, Webcam, Quality


def parse_args(docopt_args: Dict) -> Configuration:
    now = normalized_now()
    args = Configuration(now, now)
    if docopt_args['--time']:
        new_time = parse_raw_time(now, docopt_args['--time'])
        if new_time:
            args.time = new_time
        else:
            print(f'cannot read --time {docopt_args["--time"]} is in incorrect format')
        if docopt_args['--last']:
            new_time = parse_raw_time(now, docopt_args['--time'])
            if new_time:
                args.time = new_time
            else:
                print(f'cannot read --last {docopt_args["--last"]} is in incorrect format')

    if docopt_args['--interval']:
        if docopt_args['--interval'] % 10 != 0:
            print('interval {} invalid. using default {}'.format(
                docopt_args['--interval'],
                args.interval,
            ))

    if docopt_args['--webcam']:
        for w in Webcam:
            if str(docopt_args['--webcam']) == w.name.lower():
                args.webcam = w

    if docopt_args['--quality']:
        for q in Quality:
            if str(docopt_args['--quality']) == q.name.lower():
                args.quality = q

    return args
