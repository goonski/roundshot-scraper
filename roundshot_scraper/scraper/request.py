import calendar
import os
import time
from datetime import datetime
from pathlib import Path
from shutil import copyfileobj
from threading import Thread

import requests

from roundshot_scraper.types import Webcam, Quality


def create_url(dt: datetime, webcam: Webcam, quality: Quality):
    """
    creating an url

    url is formatted as follows
    https://storage.roundshot.com/5595515f75aba9.83008277/2021-10-11/10-10-00/2021-10-11-10-10-00_full.jpg
    https://storage.roundshot.com/5595515f75aba9.83008277/2022-09-24/14-30-00/2022-09-24-14-30-00_full.jpg

    :param webcam: Webcam object
    :param dt: datetime at which the image has been made
    :param quality: the Quality of the image
    :return:
    """
    date_fragment = f'{dt.year}-{dt.month:02}-{dt.day:02}'
    time_fragment = f'{dt.hour:02}-{dt.minute:02}-00'

    # stitching the url together
    full_url = '{}/{}/{}/{}-{}_{}.jpg'.format(
        webcam.value,
        date_fragment,
        time_fragment,
        date_fragment,
        time_fragment,
        quality.value,
    )
    return full_url


class ThreadedFetcher(Thread):
    # TODO: maybe create method to download, rather than always creating a new instance
    def __init__(self, time: datetime, webcam: Webcam, quality: Quality):
        Thread.__init__(self, name='ThreadedFetcher')
        self.time = time
        self.url = create_url(time, webcam, quality)
        self.webcam = webcam
        self.quality = quality

    def run(self) -> None:
        path = Path(
            '{}-{}/{}'.format(
                self.time.year,
                self.time.month,
                self.time.day,
            )
        )
        os.makedirs(path, exist_ok=True)

        filename = '{}_{:02}-{:02}_{}-{}.jpg'.format(
            calendar.day_abbr[ self.time.weekday()].lower(),
            self.time.hour,
            self.time.minute,
            self.webcam.name.lower(),
            self.quality.value,
        )
        img_path = path / filename
        if os.path.exists(img_path):
            print('Skip {} (Already exist)'.format(img_path))
            return
        req_headers = {}
        req_headers['User-Agent'] = '{} {} {}'.format(
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64)',
            'AppleWebKit/537.36 (KHTML, like Gecko)',
            'Chrome/96.0.4664.110 Safari/537.36',
        )
        try:
            response = requests.get(self.url, stream=True, headers=req_headers)
            if response.status_code == 200:
                print(f'Saving Image from {self.time} under {path}')
                with open(img_path, mode='wb') as img:
                    copyfileobj(response.raw, img)
                print(f'Successfully saved Image in "{img_path}"')
            else:
                print(
                    'Could not fetch {} (status code: {})'.format(
                        self.url,
                        response.status_code
                    )
                )
            # avoid frequent request
            time.sleep(10)
            # TODO: use scrapy or proxies to avoid status code:429
        except Exception as e:
            print(f'Could not save Image from {self.time}.\n{e}')
