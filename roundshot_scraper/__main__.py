"""
Webcam Scraper

Usage:
    scraper.py
    scraper.py [--list-webcams]
    scraper.py [-t TIME] [-l END] [-i INTERVAL] [-w WEBCAM] [-q QUALITY] [-o OUTPUT]
    scraper.py [--time=TIME] [--last=END] [--interval=INTERVAL] [--webcam=WEBCAM] [--quality=QUALITY] [--output=OUTPUT]

Options:
-h --help            help
--list-webcams

-t TIME --time=TIME                 Time the image was taken
-l END --last=END                   Time of last image (if specified, will download all images from beginning time (TIME))
-i INTERVAL --interval=INTERVAL     Interval in minutes (min. 10 min, and can only be increased in 10 min steps)

-w WEBCAM --webcam=WEBCAM           Select either 'wildspitz' or 'rigi' webcam
-q QUALITY --quality=QUALITY        Quality of the Image (full, default, half, quarter, eight)

-o OUTPUT --output=OUTPUT           Parent output folder Path of images (will create subdirectories)
"""
from datetime import timedelta
from typing import List

from docopt import docopt

from roundshot_scraper.args import parse_args
from roundshot_scraper.types import Configuration, Webcam
from roundshot_scraper.scraper.request import ThreadedFetcher


def download_images(args: Configuration):
    # max 3 threads at a time
    active_threads: List = []
    webcam_time = args.time
    while webcam_time < args.end.replace(minute=args.end.minute + 1):
        if len(active_threads) < 3:
            t = ThreadedFetcher(webcam_time, args.webcam, args.quality)
            t.start()
            webcam_time += timedelta(minutes=args.interval)
            active_threads.append(t)
        else:
            active_threads[0].join()
            active_threads.pop(0)


def main():
    # argument parsing
    docopt_args = docopt(__doc__, help=True)
    args = parse_args(docopt_args)
    
    if docopt_args.get('--list-webcams', False):
        for w in Webcam:
            print(f'{w.name.lower()} ({w.value})')
    else:
        download_images(args)


if __name__ == '__main__':
    main()
